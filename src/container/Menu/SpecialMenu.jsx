import React from 'react';

import { SubHeading, MenuItem } from '../../components';
import {images, data} from '../../constants'

import './SpecialMenu.css';

const SpecialMenu = () => (
  <section 
  className="app__specialMenu 
  flex__center section__padding" id="menu">
    <div className="app__specialMenu-title">
      <SubHeading title="Меню, которое подходит тебе по вкусу" />
      <h1 className="headtext__cormorant">Специальное предложение</h1>
    </div>
    <div className="app__specialMenu-menu">
      <div className="app__specialMenu-menu-wine flex__center">
          <p className="app__specialMenu-menu-heading">Вино и пиво</p>
          <div className="app__specialMenu-menu-items">
              {data.wines.map((wine, idx) => (
                <MenuItem key={wine.title + idx}
                title={wine.title}
                price={wine.price}
                tags={wine.tags}
                />
              ))}
          </div>
      </div>
      <div className="app__specialMenu-menu-img">
          <img src={images.menu} alt="изображение коктеля"/>
      </div>
      <div className="app__specialMenu-menu-cocktails flex__center">
          <p className="app__specialMenu-menu-heading">Коктели</p>
          <div className="app__specialMenu-menu-items">
              {data.cocktails.map((cocktail, idx) => (
                  <MenuItem key={cocktail.title + idx}
                  title={cocktail.title}
                  price={cocktail.price}
                  tags={cocktail.tags} />
              ))}
          </div>
      </div>
    </div>
    <div style={{marginTop: '15px'}}>
      <button 
        type="button"
        className="custom__button"        
      >Смотреть больше</button>
    </div>
  </section>
);

export default SpecialMenu;
